const slides = document.getElementsByClassName("slide-image");
const dots = document.getElementsByClassName("dotsStyle");
for (i = 0; i <= slides.length - 1; i++) {
    let dotElement = '<span class="dotsStyle"'+ (i===0 ? ' focusedDot"':'' )+' id="dot' + i + '" data-index="' + i + '"></span>';
    document.getElementById("dotsContainer").innerHTML += dotElement;
}
function MySlider(slides){
    var that = this;
    var removeClassFromSlide = function () {
        for (i = 0; i < slides.length-1; i++) {
            var item = slides[i];
            item.classList.remove("slide-image--visible");
        }
    }
    this.slides = slides;
    this.index = 0;
    this.timer = null;
    this.start = function () {
        this.timer = setInterval(function () {
            that.sliderNext();
        }, 3000)// here you can change time of slides shift in ms. 
    }
    this.sliderNext = function () {
        $(this.slides[this.index]).removeClass("slide-image--visible");
        $(dots[this.index]).removeClass("focusedDot");
       this.index++;
        if (this.slides[this.index] === undefined) {
            this.index = 0;
        }
        $(this.slides[this.index]).addClass("slide-image--visible");
        $(dots[this.index]).addClass("focusedDot");
    }
    this.sliderPrev = function () {
        $(this.slides[this.index]).removeClass("slide-image--visible");
        $(dots[this.index]).removeClass("focusedDot");
        if (this.index === 0) {
            this.index = this.slides.length;
        }
        if (this.index != 0) {
            this.index--;
        }
        $(this.slides[this.index]).addClass("slide-image--visible");
        $(dots[this.index]).addClass("focusedDot");
    }
    $(document).on("click", ".controlBtn-next", function () {
        clearInterval();
        that.sliderNext();
    });

    $(document).on("click", ".controlBtn-prev", function () {
        clearInterval();
        that.sliderPrev();
    });

    this.focusedDot = function(dotIndex){
        $(dots[this.index]).removeClass("focusedDot");
        $(this.slides[this.index]).removeClass("slide-image--visible");
        $(dots[dotIndex]).addClass("focusedDot");
        $(this.slides[dotIndex]).addClass("slide-image--visible");
        this.index = dotIndex;
    }

    $("#dotsContainer").on("click", ".dotsStyle", function (e) {

        let dot = e.target.id;
        console.log(dot);
        let dotId =  dot.charAt(3);
        console.log(dotId);
        that.focusedDot(dotId);

    })
}
var slider = new MySlider(slides);
slider.start();
